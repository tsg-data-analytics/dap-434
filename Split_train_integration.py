# -*- coding: utf-8 -*-
"""
Created on Mon Jan 24 17:24:29 2022

@author: manuelc
"""

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import math 

import datetime
from sklearn.model_selection import train_test_split

import json
from datetime import datetime							 



#########################################################################################
#                                                                                       #
#                                   LOAD DATASETS                                       #
#                                                                                       #
#########################################################################################

filename = 'MPD_All_Data_202001.csv'
mpd_data = pd.read_csv(filename)

mpd_data_2021 = mpd_data[(mpd_data['ReceivedDte'] >= '2021-01-01') & \
                         (mpd_data['ReceivedDte'] <= '2022-01-14')
                         ]

X = mpd_data_2021['ApplicantID']
y = mpd_data_2021['ISZPD']

x_train, x_integration, y_train, y_integration = train_test_split(X, y, test_size=0.3, random_state=0, stratify=y)

y.value_counts()
y_train.value_counts()
y_integration.value_counts()

integration_data = {'ApplicantId': x_integration, 
                    'IsZPD': y_integration
                    }

integration_data = pd.DataFrame(integration_data)

integration_data.to_csv('Integration_ApplicantIds_20200101-20210114.csv')